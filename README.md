# Arduino-DHT22-2018

Code for operating DHT22 humidity and temperature sensor with Arduino and 4-digit display, updated for Arduino UNOs as of 2018 based on @ejgertz's code: https://github.com/ejgertz/Arduino-DHT22

## Usage

The code here is used in conjunction with Chapter 7 "Humidity, Temperature & Dew Point/4Char Display" from the book *Environmental monitoring with Arduino*. Full citation:

Justo, P., & Gertz, E. (2012). *Environmental monitoring with Arduino* (1st ed.). Sebastopol, California, United States: O’Reilly Media.

## License

The update was done by [Steve Symons](https://www.watershed.co.uk/studio/residents/steve-symons), and this repository is shared under the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.